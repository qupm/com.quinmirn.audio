# Audio System

Simple Audio System for Unity.

## Installation

### Requirement

* Unity 2021.3 or later

### Using Unity Packager Manager

``` json
{
    "dependencies": {
        "com.quinmirn.audio": "https://gitlab.com/qupm/com.quinmirn.audio.git"
    }
}
```

## Setup

The system is fairly easy to setup.

1. Create a new `GameObject` and attach `AudioSystem` component to it.
2. Create a new `GameObject` and attach `SoundEmitter` component to it. The `SoundEmitter` component requires an `AudioSource` for it to work. Make it a prefab.
3. Assign the `SoundEmitter` prefab to the `_soundEmitterPrefab` field.
4. Add audio data.

There's a sample `AudioSystem` and `SoundEmitter` prefabs inside the `StandardAssets/Prefabs` folder.

## Usage

### Playing Audio

The system will get if the passed key is present before playing it.

``` c#
AudioPlayCue.Raise("YOUR_AUDIO_KEY");
```

### Changing Volume

Changing volume only works if you have `AudioMixer` setup in your `AudioSystem` instance.

``` c#
// Changing master volume to 1.0
SetAudioVolume.Master(1.0F);
```