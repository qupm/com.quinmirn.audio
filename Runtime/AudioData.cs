﻿using System;
using UnityEngine;
using UnityEngine.Audio;
using Random = UnityEngine.Random;

namespace Quinmirn.Audio
{
    [Serializable]
    public sealed class AudioData
    {
        [Header("General Settings")]
        [SerializeField] private string _key;

        public string Key => _key;
        
        [SerializeField] private AudioClip _clip;

        public AudioClip Clip => _clip;

        [Header("Playback Settings")]
        [SerializeField] private AudioMixerGroup _output;

        public AudioMixerGroup Output => _output;
        
        [Range(0.0f, 1.0f)]
        [SerializeField] private float _volume = 1.0f;

        public float Volume => _volume;
        
        [SerializeField] private bool _loop = false;

        public bool Loop => _loop;

        [SerializeField] private bool _randomPitch = false;
        
        [Range(-3.0f, 3.0f)]
        [SerializeField] private float _pitch = 1.0f;

        public float Pitch
        {
            get
            {
                if (_randomPitch)
                {
                    return Random.Range(1.0f, 3.0f);
                }

                return _pitch;
            }
        }
    }
}