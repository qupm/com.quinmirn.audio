﻿using System;
using UnityEngine;

namespace Quinmirn.Audio
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(AudioSource))]
    internal sealed class SoundEmitter : MonoBehaviour
    {
        private AudioSource _source;

        public event Action<SoundEmitter> Completed;

        private void Awake()
        {
            _source = GetComponent<AudioSource>();
            _source.loop = false;
            _source.clip = null;
        }

        private void OnDestroy()
        {
            Completed = null;
        }

        public void Play(AudioData audioData)
        {
            _source.outputAudioMixerGroup = audioData.Output;
            _source.clip = audioData.Clip;
            _source.volume = audioData.Volume;
            _source.loop = audioData.Loop;
            _source.pitch = audioData.Pitch;
            
            _source.Play();

            if (audioData.Loop)
            {
                return;
            }
            
            Invoke(nameof(Complete), audioData.Clip.length);
        }

        public void Stop()
        {
            Complete();
        }

        private void Complete()
        {
            Completed?.Invoke(this);
        }
    }
}