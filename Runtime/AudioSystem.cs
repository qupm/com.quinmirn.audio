using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;

namespace Quinmirn.Audio
{
    [DisallowMultipleComponent]
    public sealed class AudioSystem : MonoBehaviour
    {
        [Header("General Settings")]
        [SerializeField] private AudioMixer _audioMixer;
        
        [Header("Sound Emitter Settings")]
        [SerializeField] private SoundEmitter _soundEmitterPrefab;

        [Min(1)]
        [SerializeField] private int _soundEmitterCount = 10;
        
        private SoundEmitterPool _soundEmitterPool;
        
        [Header("Audio Data")]
        
        [SerializeField] private AudioData[] _audios = Array.Empty<AudioData>();

        private Dictionary<string, AudioData> _audioMap;

        private void Start()
        {
            _soundEmitterPool = new SoundEmitterPool(_soundEmitterPrefab, transform, _soundEmitterCount);

            _audioMap = _audios.ToDictionary(a => a.Key);
        }

        private void OnEnable()
        {
            AudioPlayCue.Raised += OnPlayAudioCue;
            SetAudioVolume.Raised += OnAudioVolumeChanged;
        }

        private void OnDisable()
        {
            AudioPlayCue.Raised -= OnPlayAudioCue;
            SetAudioVolume.Raised -= OnAudioVolumeChanged;
        }

        private void OnAudioVolumeChanged(SetAudioVolume eventData)
        {
            _audioMixer.SetFloat(eventData.Key, NormalizedToMixerValue(eventData.Volume));
        }

        private void OnPlayAudioCue(AudioPlayCue eventData)
        {
            if (!_audioMap.TryGetValue(eventData.Key, out var audioData))
            {
                return;
            }
            
            _soundEmitterPool.Get()
                .Play(audioData);
        }

        private static float NormalizedToMixerValue(float normalizedValue)
        {
            // We're assuming the range [0 to 1] becomes [-80dB to 0dB]
            // This doesn't allow values over 0dB
            return (normalizedValue - 1f) * 80f;
        }
    }
}
