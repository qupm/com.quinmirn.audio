﻿using System;

namespace Quinmirn.Audio
{
    public readonly struct SetAudioVolume
    {
        public readonly string Key;

        public readonly float Volume;

        public static event Action<SetAudioVolume> Raised; 

        private SetAudioVolume(string key, float volume) : this()
        {
            Key = key;
            Volume = volume;
        }

        public static void Master(float volume) => Raise("MasterVolume", volume);
        
        public static void Music(float volume) => Raise("MusicVolume", volume);
        
        public static void Sfx(float volume) => Raise("EffectVolume", volume);

        private static void Raise(string key, float volume)
        {
            Raised?.Invoke(new SetAudioVolume(key, volume));
        }
    }
}