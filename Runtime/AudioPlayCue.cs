﻿using System;

namespace Quinmirn.Audio
{
    public readonly struct AudioPlayCue
    {
        public readonly string Key;

        public static event Action<AudioPlayCue> Raised; 

        private AudioPlayCue(string key) : this()
        {
            Key = key;
        }

        public static void Raise(string key)
        {
            Raised?.Invoke(new AudioPlayCue(key));
        }
    }
}