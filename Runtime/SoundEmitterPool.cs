﻿using System;
using UnityEngine;
using UnityEngine.Pool;
using Object = UnityEngine.Object;

namespace Quinmirn.Audio
{
    internal sealed class SoundEmitterPool : IDisposable
    {
        private ObjectPool<SoundEmitter> _pool;

        private SoundEmitter _prefab;

        private Transform _parent;
        
        public SoundEmitterPool(SoundEmitter prefab, Transform parent, int defaultCapacity)
        {
            _prefab = prefab;

            _parent = parent;

            _pool = new ObjectPool<SoundEmitter>(OnCreateObject, 
                OnGetObject, 
                OnReleaseObject, 
                OnDestroyObject,
                true, 
                defaultCapacity);
        }

        public void Dispose()
        {
            _prefab = null;
            _parent = null;
            _pool = null;
        }

        public SoundEmitter Get()
        {
            return _pool.Get();
        }

        private void Release(SoundEmitter soundEmitter)
        {
            _pool.Release(soundEmitter);
        }

        private SoundEmitter OnCreateObject()
        { 
            var soundEmitter = Object.Instantiate(this._prefab, this._parent);
            soundEmitter.Completed += Release;
            
            return soundEmitter;
        }

        private void OnGetObject(SoundEmitter soundEmitter)
        {
            soundEmitter.gameObject.SetActive(true);
        }

        private void OnReleaseObject(SoundEmitter soundEmitter)
        {
            soundEmitter.gameObject.SetActive(false);
        }

        private void OnDestroyObject(SoundEmitter soundEmitter)
        {
            Object.Destroy(soundEmitter.gameObject);
        }
    }
}